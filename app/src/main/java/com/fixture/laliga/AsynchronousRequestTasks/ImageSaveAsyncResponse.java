package com.fixture.laliga.AsynchronousRequestTasks;

/**
 * Created by sdave on 4/21/2015.
 */
public interface ImageSaveAsyncResponse {
    void processFinish(String response);

}
